<?php

namespace App;
use App\Department;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Department extends Model
{
    
    protected $fillable=['name'];

    public function users(){
        return $this->hasMany('App\User');  
}
}