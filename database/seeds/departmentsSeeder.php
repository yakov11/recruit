<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class departmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments=[['id' => 1, 'name' => 'HR','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
        ['id' => 2, 'name' => 'Manegement','created_at' => Carbon::now(),'updated_at' =>Carbon::now()]];
        
        DB::table('departments')->insert($departments);
    }
}
